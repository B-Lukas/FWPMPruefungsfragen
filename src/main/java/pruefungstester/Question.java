package pruefungstester;

public class Question {

    final int id;
    final String Question;
    final String Answer;

    /**
     * This is a simple constructor for Questions.
     *
     * @param frage The question as a String
     */
    public Question(final String frage, final String answer) {
        this.Question = frage;
        this.Answer = answer;
        //TODO: autoincrement id when a Question is constructed
        this.id = 0;

    }
}
